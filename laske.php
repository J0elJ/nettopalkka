<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nettopalkka</title>
</head>
<body>
    <h3>Nettopalkka</h3>
    <?php
        $brutto = filter_input(INPUT_POST,'brutto',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $ennakko = filter_input(INPUT_POST,'ennakko',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $elake = filter_input(INPUT_POST,'elake',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $vakuutus = filter_input(INPUT_POST,'vakuutus',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
        $ennakko1 = $brutto / 100 * $ennakko;
        $elake1 = $brutto / 100 * $elake;
        $vakuutus1 = $brutto / 100 * $vakuutus;
        
        $netto = $brutto - $ennakko1 - $elake1 - $vakuutus1;
        
        printf("<p>Ennakkopidätys = %.2f € Työeläkemaksu = %.2f € Työttömyysvakuutusmaksu = %.2f €</p>",$ennakko1, $elake1, $vakuutus1);
        printf("<p>Nettopalkka on %.2f €</p>",$netto);
    ?>

    <a href="index.html">Laske uudestaan</a>
</body>
</html>